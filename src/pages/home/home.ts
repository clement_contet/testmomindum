import { Component, NgZone } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import 'rxjs/add/operator/map';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    protected articleHtml: SafeHtml = null

    constructor(public navCtrl: NavController, public http: HttpClient, public zone: NgZone, public sanitizer: DomSanitizer) {
        this.zone.run(() =>
            this.http.get("https://test.momindum.com/ios.html", { responseType: 'text' })
                .map(finalHtmlCode => this.sanitizer.bypassSecurityTrustHtml(finalHtmlCode))
                .subscribe(safeHtml => this.zone.run(() => this.articleHtml = safeHtml))
        );
    }
}
